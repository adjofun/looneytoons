{-|
Module      : Core
Description : Taz internal definitions
-}

{-# LANGUAGE DeriveGeneric #-}

module Core (
    TAZMachine (..)
  , TAZWord (..)
  , TAZInput (..)
  , TAZOutput (..)
  , Opcode (..)
  , TAZProgram
  , Instr (..)
  , Arg
  , Addr
  , magicnumber
  , tw2ins
  , tw2int
  , tw2flt
  , ins2tw
  , ins2int
  , ins2flt
  , int2tw
  , int2ins
  , int2flt
  , flt2tw
  , flt2ins
  , flt2int
) where

import GHC.Generics

import Imports

-- | TAZ machine consists of memory,
--   next instruction index,
--   halt flag,
--   input and output lists.
data TAZMachine = TAZM { mem :: Array Addr TAZWord
                       , addrp :: Addr
                       , isHalt :: Bool
                       , inpList :: [TAZInput]
                       , outList :: [TAZOutput]
                       } deriving (Eq, Show)

-- | TAZ word can be the TAZ instruction, Int32, or Float.
--   They all have 4 bytes, so it is just a 4-byte bytestring.
--   Note that there is no length check on the type level.
newtype TAZWord = TW ByteString deriving (Eq, Read, Generic)
instance Show TAZWord where
  show tw = printf "%s | %13d | %15e" (show (tw2ins tw)) (tw2int tw) (tw2flt tw)

-- | TAZ I/O data can be an Int32, or a Float.
data TAZInput = II Int32 | IF Float deriving (Eq, Show, Generic)
data TAZOutput = OI Int32 | OF Float deriving (Eq, Show, Generic)

-- | Alias for the editor
-- | Program for TAZ is a sequence of TAZ words, each at defined place in memory.
type TAZProgram = [(Addr, TAZWord)]

-- | TAZ opcodes.
--   See the reference and the table for their description.
data Opcode = Nop | Ini | Oui | Inf | Ouf
            | Mov | Swp | Jmp
            | Jeq | Jne | Jls | Jgs
            | Jle | Jge | Add | Sub
            | Mul | Div | Mod | And
            | Or  | Xor | Not | Shf
            | Rot | Fad | Fsu | Fmu
            | Fdi | Itf | Fti | Hlt
            | Unk
            deriving (Show, Eq, Ord, Enum, Bounded, Generic)

-- | TAZ instruction is a opcode with three arguments.
data Instr = I Opcode Arg Arg Arg deriving (Eq, Generic)

instance Show Instr where
  show (I op a1 a2 a3) = printf "%-3s  %02x  %02x  %02x" (show op) a1 a2 a3

-- | Aliases for better looking type signatures.
type Arg = Word8
type Addr = Word8

-- | First 4 bytes of the TAZ program ("TAZ!"),
--   used for validity check.
magicnumber = pack [84, 65, 90, 33]

-- | Mappings from bytestrings to values and instructions, and their inverses.
tw2ins :: TAZWord -> Instr
tw2ins (TW bs) = let (w1:w2:w3:w4:[]) = unpack bs
               in I (w8toOp w1) w2 w3 w4

ins2tw :: Instr -> TAZWord
ins2tw (I op a1 a2 a3) = TW $ pack [(optoW8 op), a1, a2, a3]

int2tw :: Int32 -> TAZWord
int2tw i = TW $ encode i

tw2int :: TAZWord -> Int32
tw2int (TW bs) = let (Right i) = decode bs in i

flt2tw :: Float -> TAZWord
flt2tw f = TW $ encode f

tw2flt :: TAZWord -> Float
tw2flt (TW bs) = let (Right f) = decode bs in f

ins2int :: Instr -> Int32
ins2int = tw2int . ins2tw

ins2flt :: Instr -> Float
ins2flt = tw2flt . ins2tw

int2ins :: Int32 -> Instr
int2ins = tw2ins . int2tw

int2flt :: Int32 -> Float
int2flt = tw2flt . int2tw

flt2ins :: Float -> Instr
flt2ins = tw2ins . flt2tw

flt2int :: Float -> Int32
flt2int = tw2int . flt2tw


-- | Mapping from byte to opcode
w8toOp :: Word8 -> Opcode
w8toOp 0x00 = Nop
w8toOp 0x01 = Ini
w8toOp 0x02 = Oui
w8toOp 0x03 = Inf
w8toOp 0x04 = Ouf
w8toOp 0x10 = Mov
w8toOp 0x11 = Swp
w8toOp 0x12 = Jmp
w8toOp 0x13 = Jeq
w8toOp 0x14 = Jne
w8toOp 0x15 = Jls
w8toOp 0x16 = Jgs
w8toOp 0x17 = Jle
w8toOp 0x18 = Jge
w8toOp 0x20 = Add
w8toOp 0x21 = Sub
w8toOp 0x22 = Mul
w8toOp 0x23 = Div
w8toOp 0x24 = Mod
w8toOp 0x30 = And
w8toOp 0x31 = Or
w8toOp 0x32 = Xor
w8toOp 0x33 = Not
w8toOp 0x34 = Shf
w8toOp 0x35 = Rot
w8toOp 0x40 = Fad
w8toOp 0x41 = Fsu
w8toOp 0x42 = Fmu
w8toOp 0x43 = Fdi
w8toOp 0x50 = Itf
w8toOp 0x51 = Fti
w8toOp 0xff = Hlt
w8toOp _ = Unk

-- | Inverse mapping from byte to opcode.
--   Notice, that opcodes and bytes ARE NOT isomorphic.
optoW8 :: Opcode -> Word8
optoW8 Nop = 0x00
optoW8 Ini = 0x01
optoW8 Oui = 0x02
optoW8 Inf = 0x03
optoW8 Ouf = 0x04
optoW8 Mov = 0x10
optoW8 Swp = 0x11
optoW8 Jmp = 0x12
optoW8 Jeq = 0x13
optoW8 Jne = 0x14
optoW8 Jls = 0x15
optoW8 Jgs = 0x16
optoW8 Jle = 0x17
optoW8 Jge = 0x18
optoW8 Add = 0x20
optoW8 Sub = 0x21
optoW8 Mul = 0x22
optoW8 Div = 0x23
optoW8 Mod = 0x24
optoW8 And = 0x30
optoW8 Or  = 0x31
optoW8 Xor = 0x32
optoW8 Not = 0x33
optoW8 Shf = 0x34
optoW8 Rot = 0x35
optoW8 Fad = 0x40
optoW8 Fsu = 0x41
optoW8 Fmu = 0x42
optoW8 Fdi = 0x43
optoW8 Itf = 0x50
optoW8 Fti = 0x51
optoW8 Hlt = 0xff
optoW8 Unk = 0x00
