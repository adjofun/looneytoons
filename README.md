###Overview
**TAZ** (abbr. *Too eAZy*) is an low-level-like interpreter.
It is being made primarily for teaching purposes.

A program for TAZ is a sequence of 4-byte words, which can be instructions or values.
Maximum size of the program is 1KB -> 255 instructions and values.
TAZ instruction consists of an opcode and three args, each a byte long.
TAZ args represent an address of value.
TAZ value can be an 32-bit signed integer, or a float.
TAZ loads a code and its data in the same memory space, i.e *does not* distinguish between instructions and values.
Since there are 8 bits for an opcode, there are 256 possible opcodes for TAZ.


**BugsBunny** is a editor for TAZ programs. Currently builds only under Linux.

**LooneyToons** is a web app, combining TAZ and BugsBunny into one pack.


###Usage

####TAZ
TAZ is intended to be used as interpreter.
Just supply your tazprogram, possibly with input, and see output of your program.
To evoke TAZ with no input file:
`TAZ -n tazprogram`
To dump memory space before and after execution:
`TAZ -d tazprogram`

Format for input file:
`I 42`
`F 3.1415`
`F -5.04e-6`

Please notice, that it is completly possible to write infinite loops in your tazprogram.
TAZ will not do anything against that. To halt execution, press Ctrl+C.

####BugsBunny
BugsBunny is intented to be used in couple with TAZ.
There are help on any screen of the BugsBunny by F1 key press. 

####LooneyToons
LooneyToons is intented to be the replacement of TAZ and BugsBunny on Windows.

###How to build
You will need a `stack` build system. If you are on Windows, just install Haskell Platform.
Linux distros have `stack` under different package names, consult the wiki of your distro.

Run `stack setup` to install GHC. After that, run `stack build`.
Then you can either install executables with `stack install`, and use them from command line, or execute then with `stack`.

Commands are:
`stack exec TAZ -- [options] tazprogram input`
`stack exec BugsBunny`
`stack exec LooneyToons`
