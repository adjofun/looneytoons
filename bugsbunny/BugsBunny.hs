module Main where

import Imports hiding ((//), length)
import Core
import TAZProgram
import TAZMachine


import Data.Char
import Data.List
import Data.Foldable (toList)
import System.EasyFile
import Control.Monad.Trans (liftIO)
import Data.Vector (Vector, fromList, (//))
import Graphics.Vty
import Brick
import Brick.Util
import Brick.Widgets.Center
import Brick.Widgets.Border
import Brick.Widgets.List



data Bugs = Start
          | Open (List String (FilePath, FileType)) FilePath
          | Save TAZProgram (List String (FilePath, FileType)) FilePath FilePath
          | View (List String (Addr, TAZWord))
          | Edit (List String (Addr, TAZWord)) Addr In
          | Help Bugs


data In = IInstr Word8 Opcode Addr Addr Addr
        | IInt32 Int32
        | IFloat Bool (Integer, Int)


data FileType = TAZP | Plain | Dir | Unsupported deriving (Eq, Ord, Show)


bugs = App { appDraw = bugsDraw
           , appChooseCursor = showFirstCursor
           , appHandleEvent = bugsEvents
           , appStartEvent = return
           , appAttrMap = bugsMap
           }


getDC :: FilePath -> IO (List String (FilePath, FileType))
getDC fp =
  let s2ft cd fp = do
        t <- isTAZProgram (cd </> fp)
        f <- doesFileExist (cd </> fp)
        d <- doesDirectoryExist (cd </> fp)
        return $ if t then TAZP else if f then Plain else if d then Dir else Unsupported
  in do
       dc <- getDirectoryContents fp
       let fdc = filter (\fp -> head fp /= '.') dc
       fts <- mapM (s2ft fp) fdc
       let l = list "dc" (fromList $ sort $ zip fdc fts) 1
       return l


main :: IO ()
main = do defaultMain bugs Start
          return ()


bugsMap :: Bugs -> AttrMap
bugsMap b = attrMap defAttr [ (listAttr, white `on` black)
                            , (attrName "TAZPF", black `on` green)
                            , (attrName "TAZP", green `on` black)
                            , (attrName "plainF", black `on` white)
                            , (attrName "plain", white `on` black)
                            , (attrName "dirF", black `on` cyan)
                            , (attrName "dir", cyan `on` black)
                            , (attrName "unsF", black `on` red)
                            , (attrName "uns", white `on` black)
                            , (attrName "B", blue `on` black)
                            , (attrName "U", cyan `on` black)
                            , (attrName "G", magenta `on` black)
                            , (attrName "S", green `on` black)
                            , (attrName "fire1", yellow `on` black)
                            , (attrName "fire2", red `on` black)
                            , (attrName "key", magenta `on` black)
                            , (attrName "notselected", cyan `on` black)
                            , (attrName "selected", black `on` cyan)
                            ]



bugsEvents (Help s) (VtyEvent (EvKey key [])) =
  case key of
    KEsc -> continue s
    _ -> continue $ Help s

bugsEvents s (VtyEvent (EvKey (KFun 1) [])) = continue $ Help s


bugsEvents Start (VtyEvent (EvKey key [])) = do
  case key of
    KChar 'o' -> do
      cwd <- liftIO $ getCurrentDirectory
      dc <- liftIO $ getDC cwd
      continue $ Open dc cwd
    KChar 'n' ->
      continue $ View (list "tazp" (fromList [(0, int2tw 0)]) 1)
    KEsc -> halt Start
    _ -> continue Start


bugsEvents s@(Open l fp) (VtyEvent e@(EvKey key [])) = do
  case key of
    KUp -> do nl <- handleListEvent e l
              continue $ Open nl fp

    KDown -> do nl <- handleListEvent e l
                continue $ Open nl fp

    KRight -> do
      let Just (_, (sfp, sft)) = listSelectedElement l

      cfp <- liftIO $ canonicalizePath (fp </> sfp)

      if sft == Dir then do
        dc <- liftIO $ getDC cfp
        continue $ Open dc cfp
      else continue $ s

    KLeft -> do
      let pfp = if fp == "/" then "/" else (joinPath . init . splitPath) fp
      cfp <- liftIO $ canonicalizePath pfp
      dc <- liftIO $ getDC cfp
      continue $ Open dc cfp

    KEnter -> do
      let Just (_, (sfp, sft)) = listSelectedElement l

      cfp <- liftIO $ canonicalizePath (fp </> sfp)

      if sft == TAZP then do
        tp <- liftIO $ load cfp
        case tp of
          Right p -> continue $ View (list "tazp" (fromList p) 1)
          Left err -> continue s
      else continue s

    KEsc -> continue $ Start

    _ -> continue $ s



bugsEvents s@(Save tp l fp sfp) (VtyEvent e@(EvKey key [])) = do
  case key of
    KUp -> do nl <- handleListEvent e l
              continue $ Save tp nl fp sfp

    KDown -> do nl <- handleListEvent e l
                continue $ Save tp nl fp sfp

    KEnter -> do
      cfp <- liftIO $ canonicalizePath (fp </> sfp)
      b <- liftIO $ save tp cfp
      if b then continue $ View (list "tazp" (fromList tp) 1)
      else continue $ Save tp l fp sfp

    KLeft -> do
      let pfp = if fp == "/" then "/" else (joinPath . init . splitPath) fp
      cfp <- liftIO $ canonicalizePath pfp
      dc <- liftIO $ getDC cfp
      continue $ Save tp dc cfp sfp

    KRight -> do
      let Just (_, (selfp, selft)) = listSelectedElement l

      cfp <- liftIO $ canonicalizePath (fp </> selfp)

      if selft == Dir then do
        dc <- liftIO $ getDC cfp
        continue $ Save tp dc cfp sfp
      else continue $ s


    (KChar c) -> do
      if isAlphaNum c || c == '.' then continue $ Save tp l fp (sfp ++ [c])
      else continue s

    KBS -> do
      continue $ Save tp l fp (if null sfp then sfp else init sfp)

    KEsc -> continue $ Start

    _ -> continue $ s




bugsEvents s@(View l) (VtyEvent e@(EvKey key [])) = do
  let ll = length l
  case key of
    KChar '-' -> do
      let Just i = listSelected l
      if ll < 2 || i == 0 then continue $ View l else do
        let (hi, htw) = toList l !! (i-1)
            (li, ltw) = toList l !! i
            nv = fromList (toList l) // [(i, (li, htw)), (i-1, (hi, ltw))]
            nl = listReplace nv (Just (i-1)) l
        continue $ View nl

    KChar '=' -> do
      let Just i = listSelected l
      if ll > 255 || i == (ll-1) then continue $ View l else do
        let (hi, htw) = toList l !! (i+1)
            (li, ltw) = toList l !! i
            nv = fromList (toList l) // [(i, (li, htw)), (i+1, (hi, ltw))]
            nl = listReplace nv (Just (i+1)) l
        continue $ View nl

    KBS -> do
      let Just (i, (a, stw)) = listSelectedElement l
          nl = case tw2ins stw of
                 (I Nop 0 0 0) -> if ll > 1 then listReplace (fromList (zip [0..] (snd $ unzip $ filter (\(a, _) -> fromIntegral a /= i) $ toList l))) (Just i) l else l
                 _ -> listReplace (fromList (toList l) // [(i, (a, int2tw 0))]) (Just i) l
      continue $ View nl

    KDown -> do
      let Just i = listSelected l
          nl = if (ll == 256 || i+1 /= ll) then l
               else listInsert ll (fromIntegral ll, int2tw 0) l
      continue $ View (listMoveDown nl)

    KEnter -> do
      let Just (_, (a, tw)) = listSelectedElement l
          (I op a1 a2 a3) = tw2ins tw
      continue $ Edit l a (IInstr 0 op a1 a2 a3)

    KEsc -> do
      let tp = toList l
      cwd <- liftIO $ getCurrentDirectory
      dc <- liftIO $ getDC cwd
      continue $ Save tp dc cwd "tazp"

    _ -> do
      nl <- handleListEvent e l
      continue $ View nl



bugsEvents s@(Edit l a i) (VtyEvent e@(EvKey key [])) = do
  case i of
    IInstr fn op a1 a2 a3 -> do
      case key of
        KChar '\t' ->
          continue $ Edit l a (IInt32 (ins2int (I op a1 a2 a3)))
        KLeft -> continue $ Edit l a (IInstr (if fn == 0 then 0 else (fn-1)) op a1 a2 a3)
        KRight -> continue $ Edit l a (IInstr (if fn == 3 then 3 else (fn+1)) op a1 a2 a3)
        KUp ->
          case fn of
            0 -> continue $ Edit l a (IInstr fn (if op == minBound then Hlt else pred op) a1 a2 a3)
            1 -> continue $ Edit l a (IInstr fn op (if a1 == minBound then maxBound else pred a1) a2 a3)
            2 -> continue $ Edit l a (IInstr fn op a1 (if a2 == minBound then maxBound else pred a2) a3)
            3 -> continue $ Edit l a (IInstr fn op a1 a2 (if a3 == minBound then maxBound else pred a3))
        KDown ->
          case fn of
            0 -> continue $ Edit l a (IInstr fn (if op == Hlt      then minBound else succ op) a1 a2 a3)
            1 -> continue $ Edit l a (IInstr fn op (if a1 == maxBound then minBound else succ a1) a2 a3)
            2 -> continue $ Edit l a (IInstr fn op a1 (if a2 == maxBound then minBound else succ a2) a3)
            3 -> continue $ Edit l a (IInstr fn op a1 a2 (if a3 == maxBound then minBound else succ a3))
        KEnter ->
          continue $ View (listRemove (fromIntegral (a+1)) $ listInsert (fromIntegral a) (a, ins2tw (I op a1 a2 a3)) l)
        KEsc ->
          continue $ View l
        _ -> continue s
    IInt32 int32 -> do
      case key of
        KChar '\t' ->
          continue $ Edit l a (IFloat False (decodeFloat (int2flt int32)))
        KEsc ->
          continue $ View l
        KUp ->
          continue $ Edit l a (IInt32 $ int32+1)
        KDown ->
          continue $ Edit l a (IInt32 $ int32-1)
        KBS ->
          continue $ Edit l a (IInt32 $ if int32 == (-1) then 0 else truncate $ (fromIntegral int32) / 10)
        KChar '-' ->
          continue $ Edit l a (IInt32 $ negate int32)
        KChar c ->
          if isNumber c then continue $ Edit l a (IInt32 $ read $ show int32 ++ [c]) else continue s
        KEnter ->
          continue $ View (listRemove (fromIntegral (a+1)) $ listInsert (fromIntegral a) (a, int2tw int32) l)
        _ -> continue s
    IFloat b (m, e) -> do
      case key of
        KEsc ->
          continue $ View l
        KChar '\t' -> do
          let (I op a1 a2 a3) = flt2ins ((fromIntegral m)*(2^^(fromIntegral e)))
          continue $ Edit l a (IInstr 0 op a1 a2 a3)
        KLeft -> 
          continue $ Edit l a (IFloat (not b) (m, e))
        KRight ->
          continue $ Edit l a (IFloat (not b) (m, e))
        KEnter ->
          continue $ View (listRemove (fromIntegral (a+1)) $ listInsert (fromIntegral a) (a, flt2tw ((fromIntegral m)*(2^^(fromIntegral e)))) l)
        _ -> continue s



bugsEvents s _ = continue s



bugsDraw Start =
  let b = vBox $ [ withAttr (attrName "fire1") $ str "   (  "
                 , withAttr (attrName "fire1") $ str " ( )\\ "
                 , withAttr (attrName "fire2") $ str " )((_)"
                 , withAttr (attrName "fire2") $ str "((_)_ "
                 , withAttr (attrName "B")     $ str " | _ )"
                 , withAttr (attrName "B")     $ str " | _ \\"
                 , withAttr (attrName "B")     $ str " |___/"
                 ]

      u = vBox $ [ withAttr (attrName "fire1") $ str "   )   "
                 , withAttr (attrName "fire1") $ str "    (  "
                 , withAttr (attrName "fire2") $ str "    )\\ "
                 , withAttr (attrName "fire2") $ str " _ ((_)"
                 , withAttr (attrName "U")     $ str "| | | |"
                 , withAttr (attrName "U")     $ str "| |_| |"
                 , withAttr (attrName "U")     $ str " \\___/ "
                 ]

      g = vBox $ [ withAttr (attrName "fire1") $ str " (    "
                 , withAttr (attrName "fire1") $ str " )\\ ) "
                 , withAttr (attrName "fire2") $ str "(()/( "
                 , withAttr (attrName "fire2") $ str " /(_))"
                 , withAttr (attrName "G")     $ str " / __|"
                 , withAttr (attrName "G")     $ str "| (_\\ "
                 , withAttr (attrName "G")     $ str " \\___|"
                 ]

      s = vBox $ [ withAttr (attrName "fire1") $ str " )\\ ) "
                 , withAttr (attrName "fire1") $ str "(()/( "
                 , withAttr (attrName "fire2") $ str " /(_))"
                 , withAttr (attrName "fire2") $ str "(_))  "
                 , withAttr (attrName "S")     $ str "/ __| "
                 , withAttr (attrName "S")     $ str "\\__ \\ "
                 , withAttr (attrName "S")     $ str "|___/ "
                 ]
      splash = center $ hLimit 24 $ vLimit 7 $ hBox [b, u, g, s]
      info = center $ hLimit 80 $ vBox [ hBox [withAttr (attrName "key") $ str "'n'", str " - new program"]
                                       , hBox [withAttr (attrName "key") $ str "'F1'", str " - help"]
                                       , hBox [withAttr (attrName "key") $ str "'o'", str " - open a file"]
                                       , hBox [withAttr (attrName "key") $ str "'Esc'", str " - quit the Bugs"]
                                       ]
  in [withAttr listAttr $ center $ hLimit 80 $ vBox [splash, info]]



bugsDraw (Help s) =
  let hl a s = withAttr (attrName a) (str s) in
  case s of
    Start ->
      let t = vBox [ str "This is a start screen."
                   , str "You can see there a logo and the command keys."
                   ]
      in [withAttr listAttr $ center $ hLimit 80 t]
    Open _ _ ->
      let t = vBox [ str "This is a open screen."
                   , hBox [ str "You can choose a file with "
                          , hl "TAZP" "TAZ program"
                          , str " by pressing "
                          , hl "key" "'Enter'"
                          , str " when "
                          , hl "TAZPF" "selected"
                          , str "."
                          ]
                   , hBox [ str "You can go in a "
                          , hl "dir" "directory"
                          , str " by pressing "
                          , hl "key" "Right"
                          , str " when "
                          , hl "dirF" "selected"
                          , str "."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Up"
                          , str " and "
                          , hl "key" "Down"
                          , str " for moving a selection."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Left"
                          , str " to go to parent directory."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Esc"
                          , str " to return to Start screen."
                          ]
                   ]
      in [withAttr listAttr $ center $ hLimit 80 t]
    Save _ _ _ _ ->
      let t = vBox [ str "This is a save screen."
                   , hBox [ str "You can save your program by pressing "
                          , hl "key" "'Enter'"
                          , str "."
                          ]
                   , hBox [ str "You can edit the filename with "
                          , hl "key" "alphanumeric characters"
                          , str " and "
                          , hl "key" "Backspace"
                          , str "."
                          ]
                   , hBox [ str "You can go in a "
                          , hl "dir" "directory"
                          , str " by pressing "
                          , hl "key" "Right"
                          , str " when "
                          , hl "dirF" "selected"
                          , str "."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Up"
                          , str " and "
                          , hl "key" "Down"
                          , str " for moving a selection."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Left"
                          , str " to go to parent directory."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Esc"
                          , str " to return to View screen."
                          ]
                   ]

      in [withAttr listAttr $ center $ hLimit 80 t]
    View _ ->
      let t = vBox [ str "This is a view screen."
                   , hBox [ str "Press "
                          , hl "key" "Up"
                          , str " and "
                          , hl "key" "Down"
                          , str " to navigate the memory."
                          ]
                   , hBox [ hl "key" "Down"
                          , str " also extends memory space when pressed at the memory bottom."
                          ]
                   , hBox [ hl "key" "Backspace"
                          , str " deletes memory, if it is null, or clears it, if it is not."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "'='"
                          , str " to move selected TAZWord down, and "
                          , hl "key" "'-'"
                          , str " to move it up."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Enter"
                          , str " to edit the selected memory location."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Esc"
                          , str " to save the program."
                          ]
                   ]
      in [withAttr listAttr $ center $ hLimit 80 t]
    Edit _ _ _ ->
      let t = vBox [ str "This is an edit screen."
                   , hBox [ str "Press "
                          , hl "key" "Tab"
                          , str " to switch between representations."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Up"
                          , str " to increment value."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Down"
                          , str " to decrement value."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Enter"
                          , str " to commit value to memory."
                          ]
                   , hBox [ str "Press "
                          , hl "key" "Esc"
                          , str " to discard changes to value."
                          ]
                   ]
      in [withAttr listAttr $ center $ hLimit 80 t]



bugsDraw (Open l fp) =
  let label = str fp
      dc = hCenter $ hLimit 80 $ renderList d2w True l
      d2w f (fp, ft) = let w = hCenter $ hLimit 80 $ hBox [padRight Max $ str ">", str fp, padLeft Max $ str "<"]
                           attr = case ft of
                                    TAZP -> if f then attrName "TAZPF" else attrName "TAZP"
                                    Plain -> if f then attrName "plainF" else attrName "plain"
                                    Dir -> if f then attrName "dirF" else attrName "dir"
                                    Unsupported -> if f then attrName "unsF" else attrName "uns"
                       in withAttr attr w
  in [borderWithLabel label dc]



bugsDraw (Save _ l fp sfp) =
  let label = str fp
      dc = hCenter $ hLimit 80 $ renderList d2w True l
      s = hCenter $ hLimit 80 $ withAttr listAttr $ vBox (map (hCenter . (hLimit 80) . str) ["Enter the filename:", sfp])
      d2w f (fp, ft) = let w = hCenter $ hLimit 80 $ hBox [padRight Max $ str ">", str fp, padLeft Max $ str "<"]
                           attr = case ft of
                                    TAZP -> if f then attrName "TAZPF" else attrName "TAZP"
                                    Plain -> if f then attrName "plainF" else attrName "plain"
                                    Dir -> if f then attrName "dirF" else attrName "dir"
                                    Unsupported -> if f then attrName "unsF" else attrName "uns"
                       in withAttr attr w
  in [borderWithLabel label $ vBox [dc, s]]



bugsDraw (View l) = [border $ hCenter $ hLimit 80 $ renderList tw2w True l]
  where tw2w f (a, tw) =
          let (m, e) = decodeFloat (tw2flt tw)
              s = printf "| %02x | %s | %-15d | %15d *2^ %-15d" a (show $ tw2ins tw) (tw2int tw) m e
              w = hCenter $ hLimit 80 $ str s
          in withAttr (attrName (if f then "selected" else "notselected")) w



bugsDraw (Edit l a i) = [border $ hCenter $ hLimit 80 $ renderList (tw2wi i) True l]
  where tw2wi i f (a, tw) =
          let (m, e) = decodeFloat (tw2flt tw) in
          if f then case i of
             IInstr fn op a1 a2 a3 ->
               let ins = I op a1 a2 a3
                   aw = withAttr (attrName "notselected") $ str $ printf "| %02x |" a
                   opw = withAttr (attrName (if fn == 0 then "selected" else "notselected")) $ str $ printf " %-3s " (show op)
                   a1w = withAttr (attrName (if fn == 1 then "selected" else "notselected")) $ str $ printf " %02x " a1
                   a2w = withAttr (attrName (if fn == 2 then "selected" else "notselected")) $ str $ printf " %02x " a2
                   a3w = withAttr (attrName (if fn == 3 then "selected" else "notselected")) $ str $ printf " %02x " a3
                   intw = withAttr (attrName "notselected") $ str $ printf "| %-15d |" (ins2int ins)
                   fltw = withAttr (attrName "notselected") $ str $ printf " %15d *2^ %-15d" m e
               in hCenter $ hLimit 80 $ hBox [aw, opw, a1w, a2w, a3w, intw, fltw]
             IInt32 int32 ->
                let aw = withAttr (attrName "notselected") $ str $ printf "| %02x |" a
                    insw = withAttr (attrName "notselected") $ str $ printf " %s " (show $ int2ins int32)
                    intw = withAttr (attrName "selected") $ str $ printf " %-15d " int32
                    fltw = withAttr (attrName "notselected") $ str $ printf " %15d *2^ %-15d" m e
                    sep = withAttr (attrName "notselected") $ str "|"
                in hCenter $ hLimit 80 $ hBox [aw, insw, sep, intw, sep, fltw]
             IFloat b (m, e) ->
                let float = (fromIntegral m)*2^^(fromIntegral e)
                    aw = withAttr (attrName "notselected") $ str $ printf "| %02x |" a
                    insw = withAttr (attrName "notselected") $ str $ printf " %s " (show $ flt2ins float)
                    intw = withAttr (attrName "notselected") $ str $ printf " %-15d " (flt2int float)
                    fltmw = withAttr (attrName (if b then "notselected" else "selected")) $ str $ printf " %15d" m
                    esep = withAttr (attrName "notselected") $ str " *2^ "
                    fltew = withAttr (attrName (if b then "selected" else "notselected")) $ str $ printf "%-15d" e
                    sep = withAttr (attrName "notselected") $ str "|"
                in hCenter $ hLimit 80 $ hBox [aw, insw, sep, intw, sep, fltmw, esep, fltew]

          else let w = str $ printf "| %02x | %s | %-15d | %15d *2^ %-15d" a (show $ tw2ins tw) (tw2int tw) m e
               in hCenter $ withAttr (attrName "notselected") w


