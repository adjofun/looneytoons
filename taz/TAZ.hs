{-# LANGUAGE Strict #-}

module Main where

import Data.Array
import qualified Data.ByteString as B
import qualified Data.ByteString.Char8 as B8
import System.Environment
import System.Console.GetOpt
import Text.Printf
import System.IO.Error
import Data.Attoparsec.ByteString
import Control.Applicative
import Data.Char
import Data.Int

import Core
import TAZMachine
import TAZProgram

data Flags = Dump | NoInput deriving (Eq, Show)

main :: IO ()
main = do
  (flags, (fn, inf)) <- processArgs
  tm <- loadMachine fn
  mtm <- if NoInput `elem` flags then return (Right tm) else getInput tm inf
  case mtm of
    Left err -> ioError (userError err)
    Right itm -> do
      if Dump `elem` flags then dumpMemory itm else return ()
      let etm = runMachine itm
      if Dump `elem` flags then dumpMemory etm else return ()
      output etm
                   
processArgs :: IO ([Flags], (FilePath, FilePath))
processArgs = do
  name <- getProgName
  args <- getArgs
  let opts = [ Option ['d'] ["dump"] (NoArg Dump) "Dump memory before and after execution"
             , Option ['n'] ["noinput"] (NoArg NoInput) "Do not ask for input"]
      (flags, fn, errs) = getOpt Permute opts args
  if not (null errs)
         || null fn
         || ((length fn /= 1) && NoInput `elem` flags)
         || ((length fn /= 2) && not (NoInput `elem` flags))
  then ioError (userError (concat errs ++ usageInfo ("Usage: " ++ name ++ " [OPTION...] program [inputfile]") opts))
  else return (flags, (fn!!0, fn!!1))

loadMachine :: String -> IO TAZMachine
loadMachine fn = do
  etp <- load fn
  case etp of
    Left err -> ioError (userError ("Program " ++ fn ++ " cannot be loaded. (" ++ err ++ ")"))
    Right tp -> return (ready tp) 

getInput :: TAZMachine -> FilePath -> IO (Either String TAZMachine)
getInput tm inf = catchIOError action (\e -> return (Left "IO error"))
  where action = do i <- B.readFile inf
                    let result = parseOnly input i 
                    case result of
                      Left err -> return $ Left err
                      Right il -> return $ Right $ tm {inpList = il}
        input = tazinput `sepBy'` (word8 10) -- TODO: better parser for TAZInput
        tazinput = (II <$> int32) <|> (IF <$> float) <?> "TAZInput"
        int32 = do
          word8 73
          spaces
          s <- option 0 sign
          n <- many1' digit
          return $ ((if s == 0 then id else negate).read) $ map (chr.fromIntegral) (n)
        float = do
          word8 70
          spaces
          m <- mantiss
          exp <- option [] exponent
          return $ read $ map (chr.fromIntegral) (m++exp)
        mantiss = do
          s <- option 0 sign
          n1 <- many1' digit
          d <- dot
          n2 <- many1' digit
          return (if s == 0 then (n1++d:n2) else s:(n1++d:n2))
        exponent = do
          es <- esep
          s <- option 0 sign
          n <- many1' digit
          return (if s == 0 then es:n else es:s:n)
        digit = let isDigit w = w >= 48 && w <= 57 in (satisfy isDigit) <?> "Digit"
        sign = word8 45 <?> "Sign"
        dot = word8 46 <?> "Dot"
        esep = (word8 101 <|> word8 69) <?> "E"
        spaces = many1' (word8 32)
  
dumpMemory :: TAZMachine -> IO ()
dumpMemory tm = do
  putStrLn "\n=== === === === === Memory dump === === === === === ==="
  mapM_ (\(i, tw) -> printf "| %02x | %s\n" i (show tw)) (assocs $ mem tm)
  putStrLn "+++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++ +++\n"

runMachine :: TAZMachine -> TAZMachine
runMachine tm = if isHalt tm == True then tm else runMachine (execute tm)

output :: TAZMachine -> IO ()
output tm = do
  putStrLn "+++ +++ +++ +++ +++ OUTPUT +++ +++ +++ +++ +++"
  mapM_ (\o -> case o of
                 OI int32 -> putStrLn ("Int32 " ++ show (int32)) 
                 OF float -> putStrLn ("Float " ++ show (float)) 
        ) (outList tm)
